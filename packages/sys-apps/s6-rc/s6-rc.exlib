# Copyright 2015-2019 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

export_exlib_phases src_configure src_install

SUMMARY="A dependency-based init script management system"
DESCRIPTION="
s6-rc is a suite of programs designed to help Unix distributions manage
services provided by various software packages, and automate initialization,
shutdown, and more generally changes to the machine state.

It keeps track of the complete service dependency tree and automatically
brings services up or down to reach the desired state.

In conjunction with s6, it ensures that long-lived services are
supervised, and that short-lived instructions are run in a reproducible
manner.
"
HOMEPAGE="http://skarnet.org/software/${PN}/"

if ever is_scm; then
    SCM_REPOSITORY="git://git.skarnet.org/s6-rc"
    require scm-git
else
    DOWNLOADS="http://skarnet.org/software/${PN}/${PNV}.tar.gz"
fi

LICENCES="ISC"
SLOT="0"
MYOPTIONS="
    static
"

DEPENDENCIES=""

DEFAULT_SRC_COMPILE_PARAMS=(
    CROSS_COMPILE=$(exhost --tool-prefix)
)

s6-rc_src_configure()
{
    local args=(
        --enable-shared
        --with-lib=/usr/$(exhost --target)/lib
        --with-dynlib=/usr/$(exhost --target)/lib
        $(option_enable static allstatic)
    )

    [[ $(exhost --target) == *-gnu* ]] || \
        args+=( $(option_enable static static-libc) )

    econf "${args[@]}"
}

s6-rc_src_install()
{
    default

    insinto /usr/share/doc/${PNVR}/html
    doins doc/*

    # dodoc doesn't preserve symlinks, the empty directories are there for a reason
    edo cp -Pr examples "${IMAGE}"/usr/share/doc/${PNVR}
    keepdir /usr/share/doc/${PNVR}/examples/source/sshd-4/data/rules/ip4/192.168.0.0_22
    keepdir /usr/share/doc/${PNVR}/examples/source/{sshd-4,smtpd-4,dns-cache}/data/rules/ip4/0.0.0.0_0
    keepdir /usr/share/doc/${PNVR}/examples/source/udhcpc-eth3/env

    insinto /usr/$(exhost --target)/libexec/s6-rc/scripts
    insopts -m755

    doins "${FILES}"/{rc.init,rc.shutdown,runlevel}

    alternatives_for s6-service-manager ${PN} 1000 \
        /usr/$(exhost --target)/libexec/s6-linux-init/scripts.default \
        /usr/$(exhost --target)/libexec/s6-rc/scripts
}

